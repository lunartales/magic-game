﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingWindowScript : MonoBehaviour

{
    //Slot0 is the mouse cursor
    public static int slot0 = 0;
    //
    public static int slot1 = 0;
    public static int slot2 = 2;
    public static int slot3 = 3;
    public static int slot4 = 4;
    public static int slot5 = 5;
    public static int slot6 = 6;

    //This method puts objects on the screen
    void OnGUI()
    {
        slot1 = PotionCraftingSlot(slot1, "Slot 1 ", 0.1f, 0.2f, 0.15f, 0.2f);

        slot2 = PotionCraftingSlot(slot2, "Slot 2 ", 0.1f, 0.4f, 0.15f, 0.2f);

        slot3 = PotionCraftingSlot(slot3, "Slot 3 ", 0.1f, 0.6f, 0.15f, 0.2f);

    }

    int PotionCraftingSlot(int Ingredient, string CraftingSlot, float x, float y, float width, float height)
    {
        return InventorySlot(Ingredient, CraftingSlot, x, y, width, height);
        //update crafting output slot:

    }

    int InventorySlot(int Ingredient, string CraftingSlot, float x, float y, float width, float height)
    {
        if (GUI.Button(scaledRect(x, y, width, height), CraftingSlot + Ingredient))
        {
            int TempSlot = slot0;
            slot0 = Ingredient;
            return TempSlot;

        }
        return Ingredient;
    }

    //This method determines the position and shape of an object on screen
    public static Rect scaledRect(float x, float y, float width, float height)
    {
        return new Rect(Screen.width * x, Screen.height * y, Screen.width * width, Screen.height * height);
    }


}


/* class CraftingWindowScript
{
    static void Main()
    {
        PotionIngredient Ingredient = new object();

    }
}

    /*public static int[] craftGrid = new int[] { -1, -1 };
    //this grid has 2 slots. -1 is assumed to be an empty slot.

    public static int whitePowder = 420;
    public static int waterPotion = 69;
    public static int magicPotion = 42;

    public static int[] magicPotionRecipe = new int[] { whitePowder, waterPotion };

    public void checkCrafting()
    {
        if (craftGrid == magicPotionRecipe)
        {
            print(magicPotion);
        }
    }
    

    public static bool IsKeyDown(System.ConsoleKey.Input.Key key);

    public void FixedUpdate()
    {
        if (Keyboard.IsKeyDown(Key.Return))
        {
            
        }    
    }
*/
    /*this is called on 'physics' tick, there's a way to control the rate of this based on 
    real life time scaling.
    Normal update is called every frame.
    public void FixedUpdate()

        

        if (Keyboard.KeyDown(Keyboard.KEY_P)) {
            craftGrid = new int[] { whitePowder, waterPotion };
        }
        else
        {
            craftGrid = new int[] { -1, -1 };
        }

        checkCrafting(); */

   