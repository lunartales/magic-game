﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingListScript: MonoBehaviour
{
    //Potions List
    public static int SmallHealthPotion = 1;
    public static int MediumHealthPotion = 2;
    public static int LargeHealthPotion = 3;

    public static int SmallManaPotion = 4;
    public static int MediumManaPotion = 5;
    public static int LargeManaPotion = 6;

    public static int GoodLuckPotion = 7;
    public static int HealthRejuvenationPotion = 0;

    //Craftable Items
    public static int HealingSav = 20;
    public static int Bandage = 21;

    //Charms
    

}
