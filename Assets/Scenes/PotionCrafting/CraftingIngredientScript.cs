﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingIngredientScript : MonoBehaviour
{
    //Ingredients will be categorized into different types:
    //Flowers, Herbs, Mushrooms, Crystals
    //Eventually after combat is added, MonsterDrops category will be added, such as
    //Slime? Bug Wings? etc.

    //Flowers
    public static int Lavender = 1;
    public static int AloeVera = 2;
    public static int Hydrangea = 3;
    public static int Carnation = 4;
    public static int Cyclamen = 5;
    public static int Lily = 6;
    public static int Ranunculus = 7;
    public static int Tulip = 8;
    //Each Flower is based on real life but has a different name in our world.


    //Herbs
    public static int Sage = 20;
    public static int Thyme = 21;
    public static int Arugula = 22;
    public static int Dandilion = 24;


    //Mushrooms
    public static int Button = 50;
    public static int Shiitake = 51;
    public static int GlowShroom = 52;

    //Crystals
    //Amber, Amethyst, Aquamarine, Citrine, Diamond, Emerald, Garnet,
    //Hematite, Jade, Jasper, Lapis, Obsidian, Opal, Peridot, Ruby,
    //Sapphire, Tiger Eye, Topaz, Tourmaline, Turquoise
    public static int Moonstone = 100;
    public static int Amethyst = 101;
    public static int Ruby = 102;
    public static int Jade = 103;

    //Runes?

    //Crafting Tools
    public static int EmptyPotionBottle = 200;
    public static int MortarPestle = 201;
    public static int Leather = 202;
    public static int LeatherStrap = 203;
}
